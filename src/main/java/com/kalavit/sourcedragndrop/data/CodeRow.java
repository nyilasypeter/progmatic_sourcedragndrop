/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kalavit.sourcedragndrop.data;

/**
 *
 * @author peti
 */
public class CodeRow {
    private String id;
    private String text;

    public CodeRow() {
    }

    public CodeRow(String id, String text) {
        this.id = id;
        this.text = text;
    }
    
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
