/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kalavit.sourcedragndrop;

import com.kalavit.sourcedragndrop.data.CodeRow;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.FileFileFilter;
import org.apache.commons.lang3.StringEscapeUtils;

/**
 *
 * @author peti
 */
public class HtmlCreator {

    private static final String MY_FOLDER = "/home/peti/programok/oktatas/progmatic/SourceDragNDrop";
    private static final String HTML_OUTPUT_FOLDER = "/home/peti/programok/oktatas/progmatic/slicedCode";

    public static void main(String[] args) {
        try {
            HtmlCreator hc = new HtmlCreator();
            hc.createAll(HTML_OUTPUT_FOLDER);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void createAll(String outputFilePath) throws IOException, TemplateException {

        FileUtils.cleanDirectory(new File(outputFilePath));
        File directory = new File(MY_FOLDER + "/doc/exercises");
        File[] subdirs = directory.listFiles(File::isDirectory);

        for (File dir : subdirs) {
            String dirName = dir.getName();
            String javaFilePath = dir.getAbsolutePath() + "/" + dirName + ".java";
            String unitTestFilePath = dir.getAbsolutePath() + "/" + dirName + "Test.java";
            List<CodeRow> codeRowList = fileToCodeRowList(javaFilePath, outputFilePath);
            Collections.shuffle(codeRowList);
            createHtml(codeRowList, outputFilePath, dirName);
        }
        
        IOUtils.copy(new FileInputStream(MY_FOLDER + "/src/main/resources/my.js"), new FileOutputStream(outputFilePath + "/" + "my.js"));
    }

    public List<CodeRow> fileToCodeRowList(String inputFilePath, String outputFilePath) throws FileNotFoundException, IOException {
        File f = new File(inputFilePath);
        try (BufferedReader b = new BufferedReader(new FileReader(f))) {
            String readLine;
            String imgFileName;
            List<CodeRow> ret = new ArrayList<>();

            while ((readLine = b.readLine()) != null) {
                readLine = StringEscapeUtils.escapeHtml4(readLine);
                imgFileName = UUID.randomUUID().toString();
                ret.add(new CodeRow(imgFileName, readLine));
            }

            return ret;
        }

    }

    public void createHtml(List<CodeRow> codeRows, String outputFilePath, String exerciseName) throws IOException, TemplateException {
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_25);
        cfg.setDirectoryForTemplateLoading(new File(MY_FOLDER + "/src/main/resources"));
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        cfg.setLogTemplateExceptions(false);

        Map root = new HashMap();
        root.put("codeRows", codeRows);
        String exercise = getExerciseDescription(exerciseName);
        root.put("exerciseDescription", exercise);
        Object unitTestCode = getUniteTestCode(exerciseName);        
        root.put("unitTestCode", unitTestCode);
        root.put("exerciseName", exerciseName);

        Template temp = cfg.getTemplate("htmlTemplate.html");

        FileOutputStream fout = new FileOutputStream(outputFilePath + "/" + exerciseName + "html");
        Writer out = new OutputStreamWriter(fout);
        temp.process(root, out);
    }

    private String getExerciseDescription(String exerciseName) throws IOException {
        File f = new File(MY_FOLDER + "/doc/exercises/" + exerciseName + "/exercise.txt");
        if(!f.exists()){
            f = new File(MY_FOLDER + "/doc/exercises/" + exerciseName + "/" + exerciseName + ".txt");
        }
        String exerCise = FileUtils.readFileToString(f, "utf-8");
        exerCise = StringEscapeUtils.escapeHtml4(exerCise);
        exerCise = exerCise.replaceAll("(\r\n|\n)", "<br />");
        return exerCise;
    }

    private String getUniteTestCode(String exerciseName) throws IOException {
        File f = new File(MY_FOLDER + "/doc/exercises/" + exerciseName + "/" + exerciseName +  "Test.java");
        String unitTest = FileUtils.readFileToString(f, "utf-8");
        unitTest = StringEscapeUtils.escapeHtml4(unitTest);
        unitTest = unitTest.replaceAll("(\r\n|\n)", "<br />");
        return unitTest;
    }

   
}
