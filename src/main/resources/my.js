$(document).ready(function () {
    prettyPrint();
    $(".codeSection").sortable({
        stop: prettyPrint});
});

function prettyPrint() {
    var spans = $(".codeRow")
    var spaces = 0;
    for (var i = 0; i < spans.length; i++) {
        var act = spans[i];
        var bsClassStr = (spaces * 20) + 'px';
        $(act).css('margin-left', bsClassStr);

        var actText = act.innerText;
        if (hasBlockOpen(actText)) {
            spaces++;
        }
        if (hasBlockClose(actText)) {
            spaces--;
            if (spaces < 0) {
                spaces = 0;
            }
            if (actText == '}') {
                var bsClassStr = (spaces * 20) + 'px';
                $(act).css('margin-left', bsClassStr);
            }
        }


    }
}

function hasBlockOpen(str) {
    return str.indexOf('{') != -1;
}

function hasBlockClose(str) {
    return str.indexOf('}') != -1;
}

function submitOrder() {
    var imgArray = new Array();
    $.each($("tbody tr td span"), function (i, val) {
        imgArray.push($(val).attr('id'));
    });
    var ret = {
        'name': 'todo',
        'imgList': imgArray
    };
}