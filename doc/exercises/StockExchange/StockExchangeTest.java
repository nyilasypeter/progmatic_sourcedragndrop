package codility;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author peti
 */
public class StockExchangeTest {
    
    public StockExchangeTest() {
    }
 
    @Test
    public void testSolution() {
        testSolution(new int[]{0, 0}, 0);
        testSolution(new int[]{100, 200, 300, 220, 11, 234, 531}, 520);
        testSolution(new int[]{200, 200, 180, 170, 11, 3, 1}, 0);
        testSolution(new int[]{100, 120, 110, 90, 100, 120, 115}, 30);
    }
    
    private void testSolution(int[] A, int expResult) {
        StockExchange instance = new StockExchange();
        int result = instance.solution(A);
        assertEquals(expResult, result);
    }
    
}
