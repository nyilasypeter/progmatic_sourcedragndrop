package codility;

public class StockExchange {

    int minPrice = Integer.MAX_VALUE;
    int minPos;
    int maxPrice;
    int maxPos;
    int maxGain;


    public int solution(int[] A) {
        for (int i = 0; i < A.length; i++) {
            int currPrice = A[i];
            if(currPrice < minPrice){
                minPrice = currPrice;
                minPos = i;
                if(maxPos < minPos){
                    maxPos = minPos;
                    maxPrice = minPrice;
                }
            }
            else{
                maxPrice = currPrice;
                maxPos = i;
            }
            calcGain(A);
        }
        return maxGain;
    }
    
    private void calcGain(int[] A){
        int actGain = A[maxPos] - A[minPos];
        if(actGain > maxGain){
            maxGain = actGain;
        }
    }
}
