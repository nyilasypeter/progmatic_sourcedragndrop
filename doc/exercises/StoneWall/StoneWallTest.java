/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codility;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author peti
 */
public class StoneWallTest {
    
    public StoneWallTest() {
    }
    
       @Test
    public void testSolution() {
        testSolution(new int[]{0, 0}, 0);
        testSolution(new int[]{100, 200, 300, 220, 11, 234, 531}, 7);
        testSolution(new int[]{200, 200, 180, 170, 11, 3, 1}, 6);
        testSolution(new int[]{100, 120, 110, 90, 100, 120, 115}, 7);
    }
    
    private void testSolution(int[] A, int expResult) {
        StoneWall instance = new StoneWall();
        int result = instance.solution(A);
        assertEquals(expResult, result);
    }
    
}
