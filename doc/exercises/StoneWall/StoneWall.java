package codility;

import java.util.Stack;

public class StoneWall {
    
    Stack<Integer> stack = new Stack();
    
    public int solution(int[] H){
        int prevHeight = 0;
        int nrOfBlocksNeeded = 0;
        for(int i=0; i<H.length; i++){
            int actHeight = H[i];
            if(actHeight > prevHeight){
                nrOfBlocksNeeded ++;
                stack.push(actHeight);
            }
            else if(actHeight < prevHeight){
                while(!stack.empty() && stack.peek() > actHeight){
                    stack.pop();                    
                }
                if(stack.empty() || stack.peek()!=actHeight){
                    nrOfBlocksNeeded++;
                    stack.push(actHeight);
                }
            }
            prevHeight = actHeight;
            
        }
        return nrOfBlocksNeeded;
    }
}
