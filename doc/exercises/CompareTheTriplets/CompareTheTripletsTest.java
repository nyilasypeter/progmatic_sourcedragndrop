package forDrag;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sige
 */
public class CompareTheThripletsTest {
    
    
    public CompareTheThripletsTest() {
    }

    /**
     * Test of solve method, of class CompareTheThriplets.
     */
    @Test
    public void testSolve() {
        int a0 = 0;
        int a1 = 1;
        int a2 = 0;
        int b0 = 0;
        int b1 = 0;
        int b2 = 1;
        int[] expResult = {1,1};
        int[] result = CompareTheThriplets.solve(a0, a1, a2, b0, b1, b2);
        assertArrayEquals(expResult, result);
    }

    //Only the "solve" method is tested.
    
}

