package fibonaccidynamic;

public class Ladder {

    public static void main(String[] args) {
        int[] A = {4,4,5,5,1,6,7,8,9,10};
        int[] B = {3,2,4,3,1,3,4,5,4,4};
        int[] S = (solution(A, B));
        for(int i=0;i<S.length;i++){
            System.out.println(S[i]);
        }
    }
    
    public static int[] solution(int[] A, int[] B){
        int ret[] = new int[A.length];
        int max = 0;
        for (int i=0;i<A.length;i++){
            max = Math.max(A[i],max);
        }
        long[] fib = new long[max+1];
        fib[0] = 1;
        fib[1] = 1;
        for(int i=2;i<max+1;i++){
            fib[i] = fib[i-1] + fib[i-2] % (1 << 30);
        }
        for (int i=0;i<A.length;i++){
            ret[i] = (int)(fib[A[i]] % (int)Math.pow(2,B[i]));
        }
        return ret;
    }
}
