/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibonaccidynamic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Krisz
 */
public class LadderTest {
    
    public LadderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSolution() {
        System.out.println("solution");
        int[] A = {4,4,5,5,1,6,7,8,9,10};
        int[] B = {3,2,4,3,1,3,4,5,4,4};
        int[] expResult = {5,1,8,0,1,5,5,2,7,9};
        int[] result = Ladder.solution(A, B);
        assertArrayEquals(expResult, result); 
    }
    
}
